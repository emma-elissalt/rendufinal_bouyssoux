from setuptools import setup
import setuptools

setup(
    name='TestSimpleCalculatorBx',
    version='0.0.1',
    author="Paul Bouyssoux",
    description="TestSimpleCalculator is a simple package \
    in order to make some test on packaging principles in Python",
    license='GNU GPLv3',
    python_requires ='>=3.4',
    package_dir={"": "Calculator"},
    packages=setuptools.find_namespace_packages(where="Calculator"),
)
